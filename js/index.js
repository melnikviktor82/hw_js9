"use strict";

// # Теоретичні питання

// 1. Опишіть, як можна створити новий HTML тег на сторінці.

// За допомогою метода document.createElement(tag) створити, передати в змінну та методами вставки вставити в DOM-дерево.

// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

// Перший параметр функціі визначає, куди додається DOM-елемент відносно DOM-елемента до якого застосовується метод.
// Це може бути: beforebegin (перед елементом), afterbegin (всередині напочатку елемента),
// beforeend (всередині вкінці елемента), afterend (після елемента).

// 3. Як можна видалити елемент зі сторінки?

// Методом .remove().

// ## Завдання

// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку.
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// #### Технічні вимоги:

// - Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент,
// до якого буде прикріплений список (по дефолту має бути document.body.
// - кожен із елементів масиву вивести на сторінку у вигляді пункту списку;

// Приклади масивів, які можна виводити на екран:

// ```javascript
// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ```

// ```javascript
// ["1", "2", "3", "sea", "user", 23];
// ```

// - Можна взяти будь-який інший масив.

// #### Необов'язкове завдання підвищеної складності:

// 1. Додайте обробку вкладених масивів. Якщо всередині масиву одним із елементів буде ще один масив, виводити його як вкладений список.
//    Приклад такого масиву:

//    ```javascript
//    ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
//    ```

//    > Підказка: використовуйте map для обходу масиву та рекурсію, щоб обробити вкладені масиви.

// 2. Очистити сторінку через 3 секунди. Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.

let div = document.createElement("div");
document.body.append(div);

function createList(arr, parent = document.body) {
  let list = document.createElement("ol");
  for (const el of arr) {
    let listItem = document.createElement("li");
    listItem.innerText = el;
    list.append(listItem);

    if (Array.isArray(el)) {
      let parentListItem = listItem.previousSibling;
      createList(el, parentListItem);
      listItem.remove();
    }

    parent.append(list);
  }
}
createList(
  ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"],
  div
);

setTimeout(() => {
    div.remove()
}, 3000);